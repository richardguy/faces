import React from 'react';
import Tilt from 'react-tilt';
import logo from './logo.png';
import './Logo.css';

const Logo = () => {
	return (
		<div className='ma4 mt0'>
		  <Tilt className="Tilt" options={{ max : 45 }} style={{ height: 70, width: 70 }} >
 		  <div className="Tilt-inner"><img style={{paddingTop: '5px'}} alt='logo' src={logo}></img></div>
		  </Tilt>
		</div>
		);
}

export default Logo;